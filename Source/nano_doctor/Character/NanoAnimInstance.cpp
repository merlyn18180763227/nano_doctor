// Fill out your copyright notice in the Description page of Project Settings.


#include "NanoAnimInstance.h"

#include "NanoCharacter.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "nano_doctor/Weapon/Weapon.h"

void UNanoAnimInstance::NativeInitializeAnimation()
{
	Super::NativeInitializeAnimation();

	NanoCharacter = Cast<ANanoCharacter>(TryGetPawnOwner());
}

void UNanoAnimInstance::NativeUpdateAnimation(float DeltaTime)
{
	Super::NativeUpdateAnimation(DeltaTime);

	if (NanoCharacter == nullptr)
	{
		NanoCharacter = Cast<ANanoCharacter>(TryGetPawnOwner());
	}
	if (NanoCharacter == nullptr) return;

	Velocity = NanoCharacter->GetVelocity();
	Speed = Velocity.Size2D();
	Direction = CalculateDirection(Velocity, NanoCharacter->GetActorRotation());

	bIsMoving = Speed > 1.0f ? true : false;

	bIsInAir = NanoCharacter->GetCharacterMovement()->IsFalling();
	bIsAccelerating = NanoCharacter->GetCharacterMovement()->GetCurrentAcceleration().Size() > 0.f ? true : false;

	bWeaponEquipped = NanoCharacter->IsWeaponEquipped();
	EquippedWeapon = NanoCharacter->GetEquippedWeapon();
	bAiming = NanoCharacter->IsAiming();

	bDied = NanoCharacter->IsDied();
	
	AO_Yaw = NanoCharacter->GetAO_Yaw();
	AO_Pitch = NanoCharacter->GetAO_Pitch();

	if (bWeaponEquipped && EquippedWeapon && EquippedWeapon->GetWeaponMesh() && NanoCharacter->GetMesh())
	{
		LeftHandTransform = EquippedWeapon->GetWeaponMesh()->GetSocketTransform(FName("LeftHandSocket"), ERelativeTransformSpace::RTS_World);
		FVector OutPosition;
		FRotator OutRotation;
		NanoCharacter->GetMesh()->TransformToBoneSpace(FName("hand_r"), LeftHandTransform.GetLocation(), FRotator::ZeroRotator, OutPosition, OutRotation);
		LeftHandTransform.SetLocation(OutPosition);
		LeftHandTransform.SetRotation(FQuat(OutRotation));

		if (NanoCharacter->IsLocallyControlled())
		{
			bLocallyControlled = true;
			FTransform RightHandTransform = EquippedWeapon->GetWeaponMesh()->GetSocketTransform(FName("hand_r"), ERelativeTransformSpace::RTS_World);
			FRotator LookAtRotation = UKismetMathLibrary::FindLookAtRotation(RightHandTransform.GetLocation(), RightHandTransform.GetLocation() + (RightHandTransform.GetLocation() - NanoCharacter->GetHitTarget()));
			RightHandRotation = FMath::RInterpTo(RightHandRotation, LookAtRotation, DeltaTime, 30.f);
		}
	}
}
