// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "NanoGameMode.generated.h"

/**
 * 
 */
UCLASS()
class NANO_DOCTOR_API ANanoGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	ANanoGameMode();
	virtual void PlayerDied(class ANanoCharacter* NanoCharacter);
	virtual void RequestRespawn(ACharacter* NanoCharacter, AController* NanoController);

};
