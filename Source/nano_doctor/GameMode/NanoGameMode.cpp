// Fill out your copyright notice in the Description page of Project Settings.


#include "NanoGameMode.h"

#include "GameFramework/PlayerStart.h"
#include "Kismet/GameplayStatics.h"
#include "nano_doctor/Character/NanoCharacter.h"

ANanoGameMode::ANanoGameMode()
{
}

void ANanoGameMode::PlayerDied(ANanoCharacter* NanoCharacter)
{
	if(NanoCharacter)
	{
		NanoCharacter->Die(false);
	}
}

void ANanoGameMode::RequestRespawn(ACharacter* NanoCharacter, AController* NanoController)
{
	if(NanoCharacter)
	{
		NanoCharacter->Reset();
		NanoCharacter->Destroy();
	}

	if(NanoController)
	{
		TArray<AActor*> PlayerStarts;
		UGameplayStatics::GetAllActorsOfClass(this, APlayerStart::StaticClass(), PlayerStarts);
		int32 Selection = FMath::RandRange(0, PlayerStarts.Num() - 1);
		RestartPlayerAtPlayerStart(NanoController, PlayerStarts[Selection]);
	}
}
