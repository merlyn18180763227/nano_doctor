// Fill out your copyright notice in the Description page of Project Settings.


#include "ProjectileWater.h"

#include "GameFramework/ProjectileMovementComponent.h"

AProjectileWater::AProjectileWater()
{
	ProjectileMovementComponent = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileMovementComponent"));
	ProjectileMovementComponent->bRotationFollowsVelocity = true;
	ProjectileMovementComponent->SetIsReplicated(true);
	ProjectileMovementComponent->InitialSpeed = WaterSpeed;
	ProjectileMovementComponent->MaxSpeed = WaterSpeed;
}

void AProjectileWater::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp,
	FVector NormalImpulse, const FHitResult& Hit)
{
	Super::OnHit(HitComp, OtherActor, OtherComp, NormalImpulse, Hit);
	UE_LOG(LogTemp, Warning, TEXT("Hit %s"), *HitComp->GetName());
}

void AProjectileWater::BeginPlay()
{
	Super::BeginPlay();
}
