// Fill out your copyright notice in the Description page of Project Settings.


#include "CharacterOverlay.h"

#include "nano_doctor/Character/NanoCharacter.h"

void UCharacterOverlay::NativeConstruct()
{
	Super::NativeConstruct();

	NanoCharacter = Cast<ANanoCharacter>(GetOwningPlayerPawn());
}
